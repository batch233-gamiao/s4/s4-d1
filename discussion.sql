-- Taylor Swift
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 5),("Red", "2012-10-22", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 402, "Pop Rock", 5),("Love Story", 355, "Country Pop", 5), ("State of Grace", 455, "Rock / alternative rock / arena rock", 6), ("Red", 341, "Country", 6);

-- Lady Gaga
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 6), ("Born This Way", "2011-05-23", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 304, "Rock and roll", 7),("Shallow", 336, "Country, rock, folk rock", 7), ("Born This Way", 420, "Electropop", 8);

-- Justin Bieber
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 7), ("Believe", "2012-06-15", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical housemoombahton", 9), ("Boyfriend", 252, "Pop", 10);

-- Ariana Grande
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 8), ("Thank U, Next", "2019-02-08", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 405, "EDM House", 11), ("Thank U, Next", 327, "Pop, R&B", 12);

-- Bruno Mars
INSERT INTO artists (name) VALUES ("Bruno Mars");
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24k Magic", "2016-11-18", 9), ("Earth to Mars", "2011-02-07", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24k Magic", 346, "Funk, disco, R&B", 13), ("Lost", 321, "Pop", 14);

/*
	1. Exclude Records
		Terminal
		Syntax
			SELECT column_name FROM table_name WHERE column_name != value;
*/
	SELECT * FROM songs WHERE id !=11;
	SELECT * FROM songs WHERE album_id !=5 AND album_id !=6;

/*
	2. Finding records using comparison operators
		Terminal
		Syntax
			SELECT * FROM table_name WHERE column_name (operator) value;
*/
	SELECT * FROM songs WHERE length < 230;
	SELECT * FROM songs WHERE length > 200;
	SELECT * FROM songs WHERE length < 230 OR length > 200;
	SELECT * FROM songs WHERE genre = "Pop";

/*
	3. Getting records with specific conditions
		Terminal
		Syntax
			-- can be used for querying multiple columns
			SELECT column_name FROM table_name WHERE condition;
			SELECT column_name FROM table_name WHERE column_name IN (values);			
*/
	SELECT * FROM songs WHERE id = 6 OR id = 7 OR id = 8;
	SELECT * FROM songs WHERE id IN (6, 7, 8);
	SELECT * FROM songs WHERE genre IN ("Pop", "Electropop", "EDM House");

/*
	4. Show records with partial match
		Terminal
			LIKE clause
			Percent(%) symbols and underscore(_) are called wildcard operators % -> represents zero or multiple characters

			Find values with a match at the start, end, or any position			
*/	
	-- Find values with a match at the start
	SELECT * FROM songs WHERE genre LIKE "rock%";
	-- Find values with a match at the start
	SELECT * FROM songs WHERE genre LIKE "%rock";
	-- Find values with a match at any position
	SELECT * FROM songs WHERE genre LIKE "%rock%";
	-- Find values with a match of a specific length/pattern
	SELECT * FROM songs WHERE song_name LIKE "__rr_";
	-- Find values with a match at certain positions
	SELECT * FROM albums WHERE album_title LIKE "_ur%";

/*
	5. Sorting Records
		Terminal
		Syntax
			SELECT column_name FROM table_name ORDER BY column_name ORDER;
				
*/		
	SELECT * FROM songs ORDER BY song_name;
	SELECT * FROM songs ORDER BY song_name ASC;
	SELECT * FROM songs ORDER BY length DESC;

/*
	6. Limiting Records
		Terminal
			SELECT * FROM songs LIMIT 5;
*/
	SELECT * FROM songs LIMIT 5;
/*
	7. Showing records with distinct values
		Terminal
			SELECT genre FROM songs;
*/
	SELECT genre FROM songs;
	SELECT DISTINCT genre FROM songs;
/*
	8. Joining two tables
		Terminal
		Syntax
			SELECT column_name FROM table1;
			JOIN table2 ON table1.id = table2.foreign_key_column;
*/
	SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/*
	9. Joining multiple tables
		Terminal
		Syntax
			SELECT column_name FROM table1;
			JOIN table2 ON table1.id = table2.foreign_key_column,
			JOIN table3 ON table2.id = table3.foreign_key_column;
*/
	SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

/*
	10. Joining tables with specified WHERE conditions		
*/
	SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE name LIKE "%a%";
/*
	11. Selecting columns to be displayed from joining tables
*/
	SELECT name, album_title, date_released, song_name, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
/*
	12. Providing aliases for joining table
		Terminal
		Syntax
			SELECT column_name AS alias FROM table1
			JOIN table2 ON table1.id = table2.foreign_key_column
			JOIN table3 ON table2.id = table3.foreign_key_column
*/
	SELECT name AS band, album_title As album, date_released,song_name As song, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

/*
	13. Displaying data from joining tables
		-- Create user information, a playlist and songs added to the user playlist		
		INSERT INTO users (username,password,full_name,contact_number,email,address) VALUES("jgamiao0221","password123","Joshua Gamiao",1123456789,"jgamiao0221@mail.com","Biñan, Laguna");

		INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2022-09-20 01:00:00");

		INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1,8),(1,14),(1,10);
*/
	-- Joining multiple tables
	SELECT * FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;

	-- Selecting specific columns to be displayed from the query
	SELECT user_id, datetime_created, song_name, length, genre, album_id FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;

	
